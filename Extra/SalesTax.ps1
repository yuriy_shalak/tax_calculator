$order = '{
    "from_country": "US",
    "from_zip": "92093",
    "from_state": "CA",
    "from_city": "La Jolla",
    "from_street": "9500 Gilman Drive",
    "to_country": "US",
    "to_zip": "90002",
    "to_state": "CA",
    "to_city": "Los Angeles",
    "to_street": "1335 E 103rd St",
    "amount": 15,
    "shipping": 1.5,
    "nexus_addresses": [
      {
        "id": "Main Location",
        "country": "US",
        "zip": "92093",
        "state": "CA",
        "city": "La Jolla",
        "street": "9500 Gilman Drive"
      }
    ],
    "line_items": [
      {
        "id": "1",
        "quantity": 1,
        "product_tax_code": "20010",
        "unit_price": 15,
        "discount": 0
      }
    ]
  }'

  $orderSerialized = '{"from_country":"US","from_zip":"92093","from_state":"CA","from_city":"La Jolla","from_street":"9500 Gilman Drive","to_country":"US","to_zip":"90002","to_state":"CA","to_city":"Los Angeles","to_street":"1335 E 103rd St","amount":15,"shipping":1.5,"nexus_addresses":[{"id":"Main Location","zip":"92093","street":"9500 Gilman Drive","country":"US","state":"CA","city":"La Jolla"}],"line_items":[{"id":"1","quantity":1,"product_tax_code":"20010","unit_price":15,"discount":0}]}'

$postContent =''

$uri = 'https://localhost:44334/api/salestax'
$body = $orderSerialized
$headers = @{
    "x-key"="ac874a8e-8bb1-4dd6-a972-68eac998f26d"; 
    "content-type" = "application/json"
} 

$postContent = Invoke-WebRequest -Uri $uri -Method Post -Headers $headers -Body (ConvertTo-Json $body) 

$postContent.Content