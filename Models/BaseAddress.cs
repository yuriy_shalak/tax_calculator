﻿using System.Text.Json.Serialization;

namespace Models
{
    public class BaseAddress
    {
        [JsonPropertyName("country")]
        public string Country { get; set; }
        [JsonPropertyName("state")]
        public string State { get; set; }
        [JsonPropertyName("city")]
        public string City { get; set; }
    }
}
