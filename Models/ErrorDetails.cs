﻿using System.Text.Json;

namespace Models
{
    public class ErrorDetails
    {
        public int StatusCode { get; set; } = 200;
        public string Message { get; set; } = "OK";
        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
