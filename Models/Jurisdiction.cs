﻿using System.Text.Json.Serialization;

namespace Models
{
    public class Jurisdiction: BaseAddress
    {
        [JsonPropertyName("county")]
        public string County { get; set; }
    }
}
