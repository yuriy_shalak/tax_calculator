﻿using System.Text.Json;
using System.Text.Json.Serialization;

namespace Models
{
    public class Location
    {
        [JsonPropertyName("zip")]
        public string Zip { get; set; }
        [JsonPropertyName("country")]
        public string Country { get; set; } = "US";
        [JsonPropertyName("state")]
        public string State { get; set; }
        [JsonPropertyName("city")]
        public string City { get; set; }
        [JsonPropertyName("street")]
        public string Street { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
