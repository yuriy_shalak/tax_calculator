﻿using System.Text.Json.Serialization;

namespace Models
{
    public class Nexus: BaseAddress
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("zip")]
        public string Zip { get; set; }
        [JsonPropertyName("street")]
        public string Street { get; set; }
    }
}
