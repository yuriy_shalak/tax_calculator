﻿using System.Text.Json.Serialization;

namespace Models
{
    public class OrderLineItem
    {
        [JsonPropertyName("id")]
        public string Id { get; set; }
        [JsonPropertyName("quantity")]
        public decimal Quantity { get; set; }
        [JsonPropertyName("product_tax_code")]
        public string ProductTaxCode { get; set; }
        [JsonPropertyName("unit_price")]
        public decimal UnitPrice { get; set; }
        [JsonPropertyName("discount")]
        public decimal Discount { get; set; }
    }
}
