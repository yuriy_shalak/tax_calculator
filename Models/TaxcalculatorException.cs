﻿using System;

namespace Models
{
    public class TaxCalculatorException: Exception
    {

        public TaxCalculatorException(string message, string method, Exception ex = null) : base(
            $"In {method}: {message}", ex) { }
    }
}
