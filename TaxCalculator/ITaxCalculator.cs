﻿using System;
using System.Threading.Tasks;
using Models;

namespace TaxCalculation
{
    public interface ITaxCalculator
    {
        Task<Tuple<Rate, ErrorDetails>> GetRateAsync(Location location, string serviceType);
        Task<Tuple<Tax, ErrorDetails>> CalculateSalesTaxForOrder(string request, string serviceType);
    }
}