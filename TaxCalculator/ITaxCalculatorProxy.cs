﻿using System.Threading.Tasks;
using Models;

namespace TaxCalculation
{
    public interface ITaxCalculatorProxy
    {
        Task<Rate> GetRateAsync(Location location);
        Task<Tax> CalculateSalesTaxForOrder(string request);
    }
}