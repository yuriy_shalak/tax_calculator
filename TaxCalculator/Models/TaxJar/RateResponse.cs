﻿using System.Text.Json.Serialization;
using Models;

namespace TaxCalculation.Models.TaxJar
{
    public class RateResponse
    {
        [JsonPropertyName("rate")]
        public Rate Rate { get; set; }
    }
}
