﻿using System.Text.Json.Serialization;
using Models;

namespace TaxCalculation.Models.TaxJar
{
    public class SalesTaxResponse
    {
        [JsonPropertyName("tax")]
        public Tax Tax { get; set; }
    }
}
