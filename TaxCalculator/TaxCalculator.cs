﻿using System;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Models;

namespace TaxCalculation
{
    public class TaxCalculator : ITaxCalculator
    {
        private readonly IConfiguration _config;
        private readonly ITaxCalculatorProxy _testTaxCalculatorProxy;

        public TaxCalculator(IConfiguration configuration)
        {
            _config = configuration;
        }

        /// <summary>
        /// Testability seam.
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="testProxy">A test tax calculation service proxy to be used in the unit testing.</param>
        public TaxCalculator(IConfiguration configuration, ITaxCalculatorProxy testProxy)
        {
            _config = configuration;
            _testTaxCalculatorProxy = testProxy;
        }

        /// <summary>
        /// TaxService proxy factory
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        private ITaxCalculatorProxy GetCalculator(string serviceType = "")
        {
            switch (serviceType)
            {
                case "TaxJar":
                    return new TaxJarCalculatorProxy(_config);

                case "test":
                    return _testTaxCalculatorProxy;

                default:
                    throw new TaxCalculatorException("Unknown serviceType requested", "TaxCalculator.GetCalculator");
            }
        }

        /// <summary>
        /// Wrapper for calling the tax rate calculator
        /// </summary>
        /// <param name="location">A <see href="Location"> to calculate the tax rate for.</see></param>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        public async Task<Tuple<Rate, ErrorDetails>> GetRateAsync(Location location, string serviceType)
        {
            if (string.IsNullOrWhiteSpace(location.Zip))
            {
                return new Tuple<Rate, ErrorDetails>(null, new ErrorDetails { Message = "Zip must be specified.", StatusCode = 400 });
            }

            var calc = GetCalculator(serviceType);
            var rate = await calc.GetRateAsync(location);
            return new Tuple<Rate, ErrorDetails>(rate, new ErrorDetails());
        }

        /// <summary>
        /// Wrapper for calling the external service to calculate the sales tax for an order.
        /// </summary>
        /// <param name="request">The request body (JSON)</param>
        /// <param name="serviceType">Type of the calculator</param>
        /// <returns></returns>
        public async Task<Tuple<Tax, ErrorDetails>> CalculateSalesTaxForOrder(string request, string serviceType)
        {
            var error = ValidateOrderSalesTaxRequest(request);
            if (error.StatusCode != 200)
            {
                return new Tuple<Tax, ErrorDetails>(null, error);
            }

            var calc = GetCalculator(serviceType);
            var tax = await calc.CalculateSalesTaxForOrder(request);
            return new Tuple<Tax, ErrorDetails>(tax, error);
        }

        /// <summary>
        /// Sales order tax request validator.
        /// Since the request is rather complex, this method can assure proper validation of the required information
        /// before invoking the external calculation service.
        /// </summary>
        /// <param name="json">The request body</param>
        /// <returns></returns>
        private ErrorDetails ValidateOrderSalesTaxRequest(string json)
        {
            var orderSalesTaxRequest = JsonSerializer.Deserialize<OrderSalesTaxRequest>(json);

            if(
                (orderSalesTaxRequest.FromCountry.ToUpper() == "US" || string.IsNullOrWhiteSpace(orderSalesTaxRequest.FromCountry))
                && 
                string.IsNullOrWhiteSpace(orderSalesTaxRequest.FromZip)
                )
            {
                return new
                    ErrorDetails
                    {
                        Message = $"Zip code must be specified if shipped from the US",
                        StatusCode = 400
                    };
            }

            if (
                (orderSalesTaxRequest.ToCountry.ToUpper() == "US" || string.IsNullOrWhiteSpace(orderSalesTaxRequest.ToCountry))
                && string.IsNullOrWhiteSpace(orderSalesTaxRequest.ToZip)
                )
            {
                return new
                    ErrorDetails
                    {
                        Message = $"Zip code must be specified if shipped to the US",
                        StatusCode = 400
                    };
            }
            
            return new ErrorDetails();
        }
    }
}
