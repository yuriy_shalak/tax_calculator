﻿using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Models;
using RestSharp;
using TaxCalculation.Models.TaxJar;


namespace TaxCalculation
{
    public class TaxJarCalculatorProxy : ITaxCalculatorProxy
    {
        private readonly RestClient _client;

        public TaxJarCalculatorProxy(IConfiguration configuration)
        {
            _client = new RestClient(configuration["TaxJar:baseUrl"]);
            _client.AddDefaultHeader("Authorization", $"Bearer {configuration["TaxJar:key"]}");
        }

        public async Task<Rate> GetRateAsync(Location location)
        {
            var restRequest = new RestRequest($"/rates/{location.Zip}");

            if (!string.IsNullOrWhiteSpace(location.City))
            {
                restRequest.AddQueryParameter("city", location.City);
            }
            if (!string.IsNullOrWhiteSpace(location.State))
            {
                restRequest.AddQueryParameter("state", location.State);
            }
            if (!string.IsNullOrWhiteSpace(location.Street))
            {
                restRequest.AddQueryParameter("street", location.Street);
            }

            var data = await _client.GetAsync<RateResponse>(restRequest).ConfigureAwait(false);
            return data.Rate;
        }

        public async Task<Tax> CalculateSalesTaxForOrder(string request)
        {
            var restRequest = new RestRequest($"/taxes", DataFormat.Json);
            restRequest.AddJsonBody(request);
            var data = await _client.PostAsync<string>(restRequest);
            return (JsonSerializer.Deserialize<SalesTaxResponse>(data)).Tax;
        }
    }

}
