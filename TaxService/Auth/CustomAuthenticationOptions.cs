﻿using Microsoft.AspNetCore.Authentication;

namespace TaxService.Auth
{
    /// <summary>
    /// Custom auth options
    /// </summary>
    public class CustomAuthenticationOptions : AuthenticationSchemeOptions
    {
        /// <summary>
        /// Customer key
        /// </summary>
        public const string X_KEY = "x-key";
        
    }
}
