﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace TaxService.Auth
{
    /// <summary>
    /// Authentication handler.
    /// </summary>
    public class ServiceAuthenticationHandler : AuthenticationHandler<AuthenticationSchemeOptions>
    {
        private readonly IConfiguration _config;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="logger"></param>
        /// <param name="encoder"></param>
        /// <param name="clock"></param>
        /// <param name="config"></param>
        public ServiceAuthenticationHandler(IOptionsMonitor<AuthenticationSchemeOptions> options, ILoggerFactory logger,
            System.Text.Encodings.Web.UrlEncoder encoder,
            ISystemClock clock, IConfiguration config)
            : base(options, logger, encoder, clock)
        {
            // in a real-world scenario an authorization provider should be injected instead
            _config = config;
        }

        /// <summary>
        /// Authentication code
        /// </summary>
        /// <returns></returns>
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            const string CLIENTS_KEY = "Clients:";
            var keyHeaders = Context.Request.Headers[CustomAuthenticationOptions.X_KEY];
            if (keyHeaders.Count > 0)
            {
                var customerKeys = _config.AsEnumerable(true).Where(x => x.Key.StartsWith(CLIENTS_KEY)).ToList();
                var client = customerKeys.FirstOrDefault(x => x.Value == keyHeaders[0]);
                if (client.Value != null)
                {
                    var identity = new ClaimsIdentity(new[] {
                            new Claim(CustomAuthenticationOptions.X_KEY, client.Key.Replace(CLIENTS_KEY,""))
                        },
                        CustomAuthenticationOptions.X_KEY);

                    var claimsPrincipal = new ClaimsPrincipal(identity);
                    Thread.CurrentPrincipal = claimsPrincipal;
                    Context.User = new GenericPrincipal(identity, null);
                    return await Task.Run(() => AuthenticateResult.Success(new AuthenticationTicket(claimsPrincipal, CustomAuthenticationOptions.X_KEY)));
                }
            }
            
            
            return await Task.Run(() => AuthenticateResult.Fail("Failed to authenticate session"));
        }
    }
}
