﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TaxCalculation;
using TaxService.Auth;

namespace TaxService.Controllers
{
    /// <inheritdoc />
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = CustomAuthenticationOptions.X_KEY)]
    public class SalesTaxController : ControllerBase
    {
        private readonly ILogger<TaxRateController> _logger;
        private readonly ITaxCalculator _taxCalculator;

        /// <summary>
        /// Calculates the sales tax for an order
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="taxCalculator"></param>
        public SalesTaxController(ILogger<TaxRateController> logger, ITaxCalculator taxCalculator)
        {
            _logger = logger;
            _taxCalculator = taxCalculator;
        }

        /// <summary>
        /// Calculate the sales tax
        /// </summary>
        /// <param name="request">The a JSON string in the TaxJar's order sales tax schema.</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] string request)
        {
            _logger.LogDebug($"Order sales tax calculation.");

            string serviceType;
            try
            {
                // this is a bit hacky: the service type is mapped off the client key. Ideally a client-to-calculator service mapping
                // provider needs to be injected and used here.
                // Also the auth key may be an optional query parameter.
                serviceType = HttpContext.User.Claims.First(x => x.Type == CustomAuthenticationOptions.X_KEY).Value;
            }
            catch (NullReferenceException)
            {
                return Unauthorized();
            }
            
            var result = await _taxCalculator.CalculateSalesTaxForOrder(request, serviceType);
            switch (result.Item2.StatusCode)
            {
                case 200:
                    _logger.LogDebug($"Order tax calculated " + result);
                    return Ok(result.Item1);
                case 400:
                    _logger.LogDebug($"Bad data: " + result.Item2.Message);
                    return BadRequest(result.Item2);
                default:
                    _logger.LogDebug($"Sales tax failed to calculate.");
                    return NoContent();
            }
        }
    }
}
