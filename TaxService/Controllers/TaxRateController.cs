﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Models;
using TaxCalculation;
using TaxService.Auth;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TaxService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = CustomAuthenticationOptions.X_KEY)]
    public class TaxRateController : ControllerBase
    {
        private readonly ILogger<TaxRateController> _logger;
        private readonly ITaxCalculator _taxCalculator;

        public TaxRateController(ILogger<TaxRateController> logger, ITaxCalculator taxCalculator)
        {
            _logger = logger;
            _taxCalculator = taxCalculator;
        }

        // GET api/<TaxRateController>/10001
        [HttpGet]
        public async Task<IActionResult> Get(string zip, string city = "", string state = "", string street = "")
        {
            _logger.LogDebug($"GET: rates for {zip}");
            var taxLocation = new Location { Zip = zip, City = city, State = state, Street = street };
            return await CalculateRate(taxLocation);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]Location taxLocation)
        {
            _logger.LogDebug($"POST: rates for {taxLocation.Zip}");
            return await CalculateRate(taxLocation);
        }

        private async Task<IActionResult> CalculateRate(Location taxLocation)
        {
            string serviceType;
            try
            {
                // this is a bit hacky: the service type is mapped off the client key. Ideally a client-to-calculator service mapping
                // provider needs to be injected and used here.
                // Also the auth key may be an optional query parameter.
                serviceType = HttpContext.User.Claims.First(x => x.Type == CustomAuthenticationOptions.X_KEY).Value;
            }
            catch
            {
                return Unauthorized();
            }

            _logger.LogInformation($"Calculating rates for {taxLocation}");

            var result = await _taxCalculator.GetRateAsync(taxLocation, serviceType);
            switch (result.Item2.StatusCode)
            {
                case 200:
                    _logger.LogDebug($"Tax rate calculated " + result.Item1);
                    return Ok(result.Item1);
                case 400:
                    _logger.LogDebug($"Bad data: " + result.Item2.Message);
                    return BadRequest(result.Item2);
                default:
                    _logger.LogDebug($"Tax rate failed to calculate.");
                    return NoContent();
            }
        }
    }
}
