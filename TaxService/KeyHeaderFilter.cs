﻿using System.Collections.Generic;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using TaxService.Auth;

namespace TaxService
{
    public class KeyHeaderFilter : IOperationFilter
    {
        /// <summary>
        /// Apply callback
        /// </summary>
        /// <param name="operation"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation != null)
            {
                if (operation.Parameters == null)
                    operation.Parameters = new List<OpenApiParameter>();


                operation.Parameters.Add(new OpenApiParameter
                {
                    Name = CustomAuthenticationOptions.X_KEY,
                    In = ParameterLocation.Header,
                    Description = "Client key",
                    Required = true,
                    Schema = new OpenApiSchema { Type = "string" }
                });
            }
        }
	}
}
