﻿using System.Security.Claims;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using TaxCalculation;
using TaxService.Auth;
using TaxService.Controllers;
using Xunit;
using Xunit.Abstractions;

namespace TaxServiceUnitTests
{
    public class SalesTaxControllerTests
    {
        private readonly XunitLogger<TaxRateController> _logger;
        private readonly IConfiguration _configuration;
        private readonly ClaimsPrincipal _authorizedTestPrincipal;

        const string SampleOrder = @"{
            ""from_country"": ""US"",
                ""from_zip"": ""92093"",
                ""from_state"": ""CA"",
                ""from_city"": ""La Jolla"",
                ""from_street"": ""9500 Gilman Drive"",
                ""to_country"": ""US"",
                ""to_zip"": ""90002"",
                ""to_state"": ""CA"",
                ""to_city"": ""Los Angeles"",
                ""to_street"": ""1335 E 103rd St"",
                ""amount"": 15,
                ""shipping"": 1.5,
                ""nexus_addresses"": [
                {
                    ""id"": ""Main Location"",
                    ""country"": ""US"",
                    ""zip"": ""92093"",
                    ""state"": ""CA"",
                    ""city"": ""La Jolla"",
                    ""street"": ""9500 Gilman Drive""
                }
                ],
                ""line_items"": [
                {
                    ""id"": ""1"",
                    ""quantity"": 1,
                    ""product_tax_code"": ""20010"",
                    ""unit_price"": 15,
                    ""discount"": 0
                }
                ]
            }";


        public SalesTaxControllerTests(ITestOutputHelper output)
        {
            _logger = new XunitLogger<TaxRateController>(output);
            _configuration = new ConfigurationBuilder().Build();

            var identity = new ClaimsIdentity(new[] {
                    new Claim(CustomAuthenticationOptions.X_KEY, "test")
                },
                CustomAuthenticationOptions.X_KEY);
            _authorizedTestPrincipal = new ClaimsPrincipal(identity);
        }

        [Fact]
        public async Task PostAsync_zipOnly_success()
        {
            var svc = new SalesTaxController(_logger, new TaxCalculator(_configuration, new ZeroCalculator()))
            {
                ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext { User = _authorizedTestPrincipal } }
            };

            var response = await svc.Post(SampleOrder);

            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            // ReSharper disable once PossibleNullReferenceException
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<Tax>(result.Value);
        }
    }
}
