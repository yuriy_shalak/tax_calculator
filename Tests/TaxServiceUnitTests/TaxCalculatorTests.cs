﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Models;
using TaxCalculation;
using Xunit;

namespace TaxServiceUnitTests
{
    public class TaxCalculatorTests
    {
        private OrderSalesTaxRequest _validOrderSalesTaxRequest;
        private readonly IConfiguration _configuration;

        public TaxCalculatorTests()
        {
            _validOrderSalesTaxRequest = new OrderSalesTaxRequest
            {
                FromCountry = "US",
                FromZip = "92093",
                FromState = "CA",
                FromCity = "La Jolla",
                FromStreet = "9500 Gilman Drive",
                ToCountry = "US",
                ToZip = "90002",
                ToState = "CA",
                ToCity = "Los Angeles",
                ToStreet = "1335 E 103rd St",
                Amount = 15,
                Shipping = (decimal)1.5,
                Nexi = new[]
                {
                    new Nexus
                    {
                        Id = "Main Location",
                        Country = "US",
                        Zip = "92093",
                        State = "CA",
                        City = "La Jolla",
                        Street = "9500 Gilman Drive"
                    },
                },
                LineItems = new[]
                {
                    new OrderLineItem {Discount = 0, Id = "1", Quantity = 1, ProductTaxCode = "20010", UnitPrice = 15}
                }
            };

            _configuration = new ConfigurationBuilder().Build();

        }

        [Fact]
        public async Task GetRateAsync_validZip_correctResults()
        {
            const string ZIP = "21136";
            var expected = new Tuple<Rate, ErrorDetails>(new Rate { Zip = ZIP, CombinedRate = 0}, new ErrorDetails());

            var calculator = new TaxCalculator(_configuration,new ZeroCalculator());

            var actual = await calculator.GetRateAsync(new Location{Zip = ZIP }, "test");

            Assert.Equal(expected.Item1.Zip, actual.Item1.Zip);
            Assert.Equal(expected.Item1.CombinedRate, actual.Item1.CombinedRate);
            Assert.Equal(expected.Item2.StatusCode, actual.Item2.StatusCode);
        }

        [Fact]
        public async Task GetRateAsync_noZip_badRequest()
        {
            const string ZIP = "";
            var expected = new Tuple<Rate, ErrorDetails>(new Rate { Zip = ZIP }, new ErrorDetails { Message = "Bad request.", StatusCode = 400 });

            var calculator = new TaxCalculator(_configuration, new ZeroCalculator());

            var actual = await calculator.GetRateAsync(new Location { Zip = ZIP }, "test");

            Assert.Null(actual.Item1);
            Assert.Equal(expected.Item2.StatusCode, actual.Item2.StatusCode);
        }

        [Fact]
        public async Task CalculateSalesTaxForOrder_validRequest_validResponse()
        {

            var calculator = new TaxCalculator(_configuration, new ZeroCalculator());

            var actual = await calculator.CalculateSalesTaxForOrder(JsonSerializer.Serialize(_validOrderSalesTaxRequest), "test");

            Assert.NotNull(actual.Item1);
            Assert.Equal(200, actual.Item2.StatusCode);
            Assert.IsType<Jurisdiction>(actual.Item1.Jurisdictions);
            Assert.IsType<TaxBreakdown>(actual.Item1.TaxBreakdown);
        }

        [Fact]
        public async Task CalculateSalesTaxForOrder_missingToUSZip_400BadResponse()
        {

            var calculator = new TaxCalculator(_configuration, new ZeroCalculator());
            var request = JsonSerializer.Deserialize<OrderSalesTaxRequest>(JsonSerializer.Serialize(_validOrderSalesTaxRequest));
            request.ToZip = "";

            var actual = await calculator.CalculateSalesTaxForOrder(JsonSerializer.Serialize(request), "test");

            Assert.Null(actual.Item1);
            Assert.Equal(400, actual.Item2.StatusCode);
            
        }

        [Fact]
        public async Task CalculateSalesTaxForOrder_missingFromUSZip_400BadResponse()
        {

            var calculator = new TaxCalculator(_configuration, new ZeroCalculator());
            var request = JsonSerializer.Deserialize<OrderSalesTaxRequest>(JsonSerializer.Serialize(_validOrderSalesTaxRequest));
            request.FromZip = "";

            var actual = await calculator.CalculateSalesTaxForOrder(JsonSerializer.Serialize(request), "test");

            Assert.Null(actual.Item1);
            Assert.Equal(400, actual.Item2.StatusCode);
        }

        [Fact]
        public async Task CalculateSalesTaxForOrder_unsupportedService_exception()
        {
            var calculator = new TaxCalculator(_configuration, new ZeroCalculator());
            await Assert.ThrowsAsync<TaxCalculatorException>(() => 
                calculator.CalculateSalesTaxForOrder(JsonSerializer.Serialize(_validOrderSalesTaxRequest)
                    , "unsupported")); // Unsupported calculator service type
        }
    }
}
