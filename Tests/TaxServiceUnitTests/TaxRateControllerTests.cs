using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Models;
using TaxCalculation;
using TaxService.Auth;
using TaxService.Controllers;
using Xunit;
using Xunit.Abstractions;

namespace TaxServiceUnitTests
{
    public class TaxRateControllerTests
    {
        private readonly XunitLogger<TaxRateController> _logger;
        private readonly IConfiguration _configuration;
        private readonly ClaimsPrincipal _authorizedTestPrincipal;
        
        public TaxRateControllerTests(ITestOutputHelper output)
        {
            _logger = new XunitLogger<TaxRateController>(output);
            _configuration = new ConfigurationBuilder().Build();

            var identity = new ClaimsIdentity(new[] {
                    new Claim(CustomAuthenticationOptions.X_KEY, "test")
                },
                CustomAuthenticationOptions.X_KEY);
            _authorizedTestPrincipal = new ClaimsPrincipal(identity);
        }

        [Fact]
        public async Task GetAsync_zipOnly_success()
        {
            var svc = new TaxRateController(_logger, new TaxCalculator(_configuration, new ZeroCalculator()))
            {
                ControllerContext = new ControllerContext {HttpContext = new DefaultHttpContext{User = _authorizedTestPrincipal } }
            };

            var response = await svc.Get(zip:"21136");

            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            // ReSharper disable once PossibleNullReferenceException
            Assert.Equal( 200, result.StatusCode);
            Assert.IsType<Rate>(result.Value);
        }
        
        [Fact]
        public async Task PostAsync_zipOnly_success()
        {
            var svc = new TaxRateController(_logger, new TaxCalculator(_configuration, new ZeroCalculator()))
            {
                ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext { User = _authorizedTestPrincipal } }
            };

            var location = new Location { Zip = "21136" };

            var response = await svc.Post(location);

            Assert.IsType<OkObjectResult>(response);
            var result = response as OkObjectResult;
            // ReSharper disable once PossibleNullReferenceException
            Assert.Equal(200, result.StatusCode);
            Assert.IsType<Rate>(result.Value);
        }

        [Fact]
        public async Task GetAsync_noZip_400BadRequest()
        {
            var svc = new TaxRateController(_logger, new TaxCalculator(_configuration, new ZeroCalculator()))
            {
                ControllerContext = new ControllerContext { HttpContext = new DefaultHttpContext { User = _authorizedTestPrincipal } }
            };

            var response = await svc.Get("");
            Assert.IsType<BadRequestObjectResult>(response);
        }

    }
}
