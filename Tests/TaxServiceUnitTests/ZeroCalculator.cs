﻿using System.Text.Json;
using System.Threading.Tasks;
using Models;
using TaxCalculation;
using TaxCalculation.Models.TaxJar;

namespace TaxServiceUnitTests
{
    /// <summary>
    /// A mock class to fill the return object with the passed location geo information and zero tax rates
    /// </summary>
    public class ZeroCalculator: ITaxCalculatorProxy
    {
        public const string TaxResponse = @"
        {
          ""tax"": {
                ""order_total_amount"": 16.5,
                ""shipping"": 1.5,
                ""taxable_amount"": 15,
                ""amount_to_collect"": 1.35,
                ""rate"": 0.09,
                ""has_nexus"": true,
                ""freight_taxable"": false,
                ""tax_source"": ""destination"",
                ""jurisdictions"": {
                    ""country"": ""US"",
                    ""state"": ""CA"",
                    ""county"": ""LOS ANGELES"",
                    ""city"": ""LOS ANGELES""
                },
                ""breakdown"": {
                    ""taxable_amount"": 15,
                    ""tax_collectable"": 1.35,
                    ""combined_tax_rate"": 0.09,
                    ""state_taxable_amount"": 15,
                    ""state_tax_rate"": 0.0625,
                    ""state_tax_collectable"": 0.94,
                    ""county_taxable_amount"": 15,
                    ""county_tax_rate"": 0.0025,
                    ""county_tax_collectable"": 0.04,
                    ""city_taxable_amount"": 0,
                    ""city_tax_rate"": 0,
                    ""city_tax_collectable"": 0,
                    ""special_district_taxable_amount"": 15,
                    ""special_tax_rate"": 0.025,
                    ""special_district_tax_collectable"": 0.38,
                    ""line_items"": [
                    {
                        ""id"": ""1"",
                        ""taxable_amount"": 15,
                        ""tax_collectable"": 1.35,
                        ""combined_tax_rate"": 0.09,
                        ""state_taxable_amount"": 15,
                        ""state_sales_tax_rate"": 0.0625,
                        ""state_amount"": 0.94,
                        ""county_taxable_amount"": 15,
                        ""county_tax_rate"": 0.0025,
                        ""county_amount"": 0.04,
                        ""city_taxable_amount"": 0,
                        ""city_tax_rate"": 0,
                        ""city_amount"": 0,
                        ""special_district_taxable_amount"": 15,
                        ""special_tax_rate"": 0.025,
                        ""special_district_amount"": 0.38
                    }
                    ]
                }
            }
        }
        ";

        public async Task<Rate> GetRateAsync(Location location)
        {
            return await Task.Run(() => new Rate
                {
                    City = location.City,
                    CityRate = 0,
                    CombinedDistrictRate = 0,
                    CombinedRate = 0,
                    County = "Zero",
                    CountyRate = 0,
                    FreightЕaxable = false,
                    State = location.State,
                    StateRate = 0,
                    Zip = location.Zip
                }
            );
        }

        public async Task<Tax> CalculateSalesTaxForOrder(string request)
        {
            var tax = JsonSerializer.Deserialize<SalesTaxResponse>(TaxResponse).Tax;
            return await Task.Run(() => tax);
        }
        
    }
}


